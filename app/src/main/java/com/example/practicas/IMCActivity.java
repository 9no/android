package com.example.practicas;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class IMCActivity extends AppCompatActivity {

    private EditText txtAltura, txtPeso;
    private TextView resultado;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    private boolean isVacio() {
        return this.txtAltura.getText().toString().equals("") || this.txtPeso.getText().toString().equals("");
    }

    private boolean isValido() {
        try {
            Double.parseDouble(this.txtAltura.getText().toString());
            Double.parseDouble(this.txtPeso.getText().toString());
        } catch (Exception err) {
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_imcactivity);
        this.init();

        this.btnCalcular.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "No dejes campos vacíos", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!isValido()) {
                    Toast.makeText(getApplicationContext(), "Hay valores incorrectos", Toast.LENGTH_SHORT).show();
                    return;
                }
                resultado.setText(String.format("%.2f", Double.parseDouble(txtPeso.getText().toString()) /
                                (Double.parseDouble(txtAltura.getText().toString()) * 2)));
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                txtPeso.setText("");
                txtAltura.setText("");
                resultado.setText("0.0");
            }
        });

        this.btnCerrar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void init() {
        this.txtAltura = (EditText) findViewById(R.id.txtAltura);
        this.txtPeso = (EditText) findViewById(R.id.txtPeso);
        this.resultado = (TextView) findViewById(R.id.resultado);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }
}