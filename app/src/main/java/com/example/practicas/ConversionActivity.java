package com.example.practicas;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ConversionActivity extends AppCompatActivity {

    private EditText txtGrados;
    private RadioGroup radioButtons;
    private RadioButton rdbCF, rdbFC;
    private TextView resultado, tipoGrado;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    private boolean isVacio() {
        return this.txtGrados.getText().toString().equals("");
    }
    private boolean isValido() {
        try {
            Double.parseDouble(this.txtGrados.getText().toString());
        } catch (Exception err) {
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_conversion);
        this.init();

        this.btnCalcular.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "Debes colocar los grados a convertir", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!isValido()) {
                    Toast.makeText(getApplicationContext(), "Hay valores incorrectos", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(radioButtons.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(getApplicationContext(), "Debes seleccionar al menos una conversión", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(rdbCF.isChecked()) {
                    resultado.setText(String.format("%.2f",
                            ((9.0 / 5.0) * Double.parseDouble(txtGrados.getText().toString())) + 32
                    ));
                    tipoGrado.setText("°F");
                }
                if(rdbFC.isChecked()) {
                    resultado.setText(String.format("%.2f",
                            (Double.parseDouble(txtGrados.getText().toString()) - 32) * (5.0 / 9.0)
                    ));
                    tipoGrado.setText("°C");
                }
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                txtGrados.setText("");
                resultado.setText("0.0");
                radioButtons.clearCheck();
            }
        });

        this.btnCerrar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void init(){
        this.txtGrados = (EditText) findViewById(R.id.txtGrados);
        this.radioButtons = (RadioGroup) findViewById(R.id.radioButtons);
        this.rdbCF = (RadioButton) findViewById(R.id.rdbCF);
        this.rdbFC = (RadioButton) findViewById(R.id.rdbFC);
        this.resultado = (TextView) findViewById(R.id.resultado);
        this.tipoGrado = (TextView) findViewById(R.id.tipoGrado);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }
}