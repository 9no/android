package com.example.practicas;

import android.content.Intent;
import android.os.Bundle;
import android.service.voice.VoiceInteractionSession;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MenuActivity extends AppCompatActivity {

    private CardView crvHola, crvIMC, crvConversion, crvMoneda, crvCotizacion, crvSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        this.iniciarComponentes();

        this.crvHola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, HolaActivity.class);
                startActivity(intent);
            }
        });

        this.crvIMC.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, IMCActivity.class);
                startActivity(intent);
            }
        });

        this.crvConversion.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ConversionActivity.class);
                startActivity(intent);
            }
        });

        this.crvMoneda.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MonedaActivity.class);
                startActivity(intent);
            }
        });

        this.crvCotizacion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, IngresaCotizacionActivity.class);
                startActivity(intent);
            }
        });

        this.crvSpinner.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, SpinnerActivity.class);
                startActivity(intent);
            }
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        this.crvHola = (CardView) findViewById(R.id.crvHola);
        this.crvIMC = (CardView) findViewById(R.id.crvIMC);
        this.crvConversion = (CardView) findViewById(R.id.crvConversion);
        this.crvMoneda = (CardView) findViewById(R.id.crvMoneda);
        this.crvCotizacion = (CardView) findViewById(R.id.crvCotizacion);
        this.crvSpinner = (CardView) findViewById(R.id.crvSpinner);
    }
}