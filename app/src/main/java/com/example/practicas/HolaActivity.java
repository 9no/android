package com.example.practicas;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class HolaActivity extends AppCompatActivity {

    private EditText txtEntrada;
    private TextView txtSalida;
    private Button btnSaludar, btnLimpiar, btnCerrar;

    private boolean isVacio() {
        return this.txtEntrada.getText().toString().equals("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_hola);
        this.init();

        this.btnSaludar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "Debes escribir tu nombre", Toast.LENGTH_SHORT).show();
                    return;
                }
                String nombre = txtEntrada.getText().toString();
                txtSalida.setText(nombre);
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                txtEntrada.setText("");
                txtSalida.setText("");
            }
        });

        this.btnCerrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void init() {
        this.txtEntrada = (EditText) findViewById(R.id.txtEntrada);
        this.txtSalida = (TextView) findViewById(R.id.txtSalida);
        this.btnSaludar = (Button) findViewById(R.id.btnSaludar);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }
}
