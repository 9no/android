package com.example.practicas;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MonedaActivity extends AppCompatActivity {
    private EditText txtPesos;
    private Spinner spnMonedas;
    private TextView total, tipo;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private int index = -1;
    private String[] tipos = {"USD", "EUR", "CAD", "GBP"};

    private boolean isVacio() {
        return this.txtPesos.getText().toString().equals("");
    }
    private boolean isValido() {
        try {
            Double.parseDouble(this.txtPesos.getText().toString());
        } catch (Exception err) {
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_moneda);
        this.init();

        // Cargar el comboBox
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.monedas, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spnMonedas.setAdapter(adapter);

        spnMonedas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                index = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        this.btnCalcular.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "Debes colocar la cantidad de Pesos a convertir", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!isValido()) {
                    Toast.makeText(getApplicationContext(), "Hay valores incorrectos", Toast.LENGTH_SHORT).show();
                    return;
                }
                switch (index) {
                    case 0:
                        total.setText(String.format("%.2f",
                                Double.parseDouble(txtPesos.getText().toString()) * 0.06
                        ));
                        break;
                    case 1:
                        total.setText(String.format("%.2f",
                                Double.parseDouble(txtPesos.getText().toString()) * 0.055
                        ));
                        break;
                    case 2:
                        total.setText(String.format("%.2f",
                                Double.parseDouble(txtPesos.getText().toString()) * 0.082
                        ));
                        break;
                    case 3:
                        total.setText(String.format("%.2f",
                                Double.parseDouble(txtPesos.getText().toString()) * 0.05
                        ));
                        break;
                }
                tipo.setText(String.valueOf(tipos[index]));
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPesos.setText("");
                total.setText("0.0");
                tipo.setText("-");
                spnMonedas.setSelection(0);
            }
        });

        this.btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void init() {
        this.txtPesos = (EditText) findViewById(R.id.txtPesos);
        this.spnMonedas = (Spinner) findViewById(R.id.spnMonedas);
        this.total = (TextView) findViewById(R.id.total);
        this.tipo = (TextView) findViewById(R.id.tipo);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }
}